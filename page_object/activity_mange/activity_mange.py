from poium import Page, PageElement

#檬太奇后台-活动管理-活动管理页面
class activity_mange(Page):
    #活动搜索功能
    activity_name = PageElement(css='.ivu-form-item:nth-of-type(1) .ivu-input-default', describe="输入活动名称")
    sreach_btn = PageElement(css='.ivu-form-label-right .ivu-btn-primary', describe="搜索按钮")
    result = PageElement(css='.ivu-table-tbody tr:nth-of-type(1) td:nth-of-type(2) span', describe="搜索结果")