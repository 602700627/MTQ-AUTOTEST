from poium import Page, PageElement,PageElements

#檬太奇后台-马甲管理-新建马甲
class waistcoat_mange(Page):
    #新建马甲
    create_button = PageElement(css='.izzue-tools span', describe="新建马甲按钮",timeout = 2)
    #创建马甲页面
    user_type = PageElements(css=".ivu-form-item-content", describe="用户类型")
    sex = PageElements(css=".ivu-radio-group ivu-radio-group-default ivu-radio-default", describe="性别")
    nickname = PageElement(css='.ivu-input-default', describe="昵称")
    signature = PageElement(css='.ivu-form-item:nth-of-type(4) .ivu-input', describe="签名")
    head_portrait = PageElement(css='.ivu-icon-ios-camera', describe="头像")
    save_button = PageElement(css='.ivu-btn-primary span', describe="保存按钮")
    reset_button = PageElement(css='.ivu-btn-default', describe="重置按钮")
