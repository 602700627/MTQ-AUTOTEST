import configparser
import codecs
import os

class ReadConfigIni():
    '''
    实例化configparser
    '''
    def __init__(self,filename):
        self.cf = configparser.ConfigParser()
        self.cf.read(filename)

    def getConfigValue(self,config,name):
        value = self.cf.get(config,name)
        return value

######################################
#验证ReadConfigIni能够运行，正式运行要注释掉或删除掉
######################################
# if __name__ == "__main__":
#
#     read_config = ReadConfigIni("d:/MTQ/config/config.ini")
#     value = read_config.getConfigValue("project","baseurl")
#     print(value)