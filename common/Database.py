import pymysql
import pymysql.cursors
from config import globalconfig

DB_url = globalconfig.DB_url
DB_user = globalconfig.DB_user
DB_pwd = globalconfig.DB_pwd
DB = globalconfig.DB
#print(DB_url,DB_user,DB_pwd,DB)

class search_db():
    def get_dbinfo(self,SQL,values):
        '''传入sql查询数据库'''
        # 连接数据库
        db = pymysql.connect(DB_url,DB_user,DB_pwd,DB,charset='utf8')
        # 得到一个可以执行SQL语句的光标对象
        cursor = db.cursor()

        try:
            # 执行SQL语句
            cursor.execute(SQL,values)
            # 获取所有记录列表
            results = cursor.fetchall()
            # 打印结果用于调试
            #print(results[0])
            # 关闭光标对象
            cursor.close()
            # 关闭数据库连接
            db.close()
            # 返回查询结果
            return results[0]
        except:
            print ("Error: unable to fetch data")

# if __name__ == '__main__':
#     nick_name = '自动化测试新建马甲iOaEFU'
#     sql = "SELECT nick_name from  user_login_info where nick_name = (%s)"
#     rsl = search_db().get_dbinfo(sql,[nick_name])
#     print(rsl)
