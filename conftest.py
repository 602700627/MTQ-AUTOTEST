# conftest.py文件
# coding:utf-8
'''
此配置文件已集成报告优化，用例失败自动截图
'''

from datetime import datetime
import html
import pytest
from selenium import webdriver
from py._xmlgen import html
from config import globalconfig

@pytest.mark.hookwrapper
def pytest_runtest_makereport(item):
    """
    当测试失败的时候，自动截图，展示到html报告中
    :param item:
    """
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    report.description = str(item.function.__doc__)
    report.nodeid = report.nodeid.encode("utf-8").decode("unicode_escape")  # 设置编码显示中文
    extra = getattr(report, 'extra', [])

    if report.when == 'call' or report.when == "setup":
        xfail = hasattr(report, 'wasxfail')
        if (report.skipped and xfail) or (report.failed and not xfail):
            file_name = report.nodeid.replace("::", "_")+".png"
            screen_img = _capture_screenshot()
            if file_name:
                html = '<div><img src="data:image/png;base64,%s" alt="screenshot" style="width:600px;height:300px;" ' \
                       'onclick="window.open(this.src)" align="right"/></div>' % screen_img
                extra.append(pytest_html.extras.html(html))
        report.extra = extra

def _capture_screenshot():
    '''
    截图保存为base64，展示到html中
    :return:
    '''
    return driver.get_screenshot_as_base64()


@pytest.mark.optionalhook
def pytest_html_results_table_header(cells):
    cells.insert(2, html.th('Description'))
    cells.insert(3, html.th('Time', class_='sortable time', col='time'))
    # cells.insert(1,html.th("Test_nodeid"))
    cells.pop()

@pytest.mark.optionalhook
def pytest_html_results_table_row(report, cells):
    cells.insert(2, html.td(report.description))
    cells.insert(3, html.td(datetime.utcnow(), class_='col-time'))
    # cells.insert(1,html.td(report.nodeid))
    cells.pop()

@pytest.mark.optionalhook
def pytest_html_results_summary(prefix, summary, postfix):
    prefix.extend([html.p("脚本作者 : 黄东炎")])


@pytest.fixture(scope='session', autouse=True)
def baseurl():
    '''
    从配置文件读取测试地址
    :return:baseurl
    '''
    baseurl = globalconfig.baseurl
    return baseurl



@pytest.fixture(scope='module', autouse=True)
def browser():
    """
    function 每一个函数或方法都会调用
    class 每一个类调用一次，一个类可以有多个方法
    module，每一个.py文件调用一次，该文件内又有多个function和class
    session 是多个文件调用一次，可以跨.py文件调用，每个.py文件就是module
    """
    global driver
    driver = webdriver.Chrome()
    yield driver
    driver.quit()
