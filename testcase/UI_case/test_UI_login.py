import pytest,os
from time import sleep
from common.read_yaml import OperationYaml
from config import globalconfig
from page_object.login_page.login_page import login_page

#从配置文件读取测试数据路径
datapath = globalconfig.UI_data
#拼接yaml文件路径
yaml = os.path.join(datapath,'login.yml')

args_item = 'url,send_data,resultstr'

#读取yaml文件加载测试数据
test_data, case_desc = OperationYaml(yaml).load_file

@pytest.mark.parametrize(args_item, test_data, ids = case_desc)
class Test_UI_login():

    @pytest.mark.UI_login
    def test_UI_login(self,browser,url,send_data,resultstr):
        '''
        测试登录

        '''
        # 打开浏览器
        page = login_page(browser)
        sleep(2)
        browser.maximize_window()
        # 打开测试地址
        page.get(url)
        page.username.send_keys(send_data['username'])
        page.password.send_keys(send_data['password'])
        page.login_box.click()
        sleep(2)
        url = page.get_url
        assert url in resultstr['resultstr']


if __name__ == '__main__':
    pytest.main(['-s', '-q','d:\\MTQ\\testcase\\UI_case\\test_UI_login.py'])
