import pytest,os
from time import sleep
from operate.business_operate import businessoperate
from common.read_yaml import OperationYaml
from config import globalconfig
from page_object.activity_mange.activity_mange import activity_mange


#从配置文件读取测试数据路径
datapath = globalconfig.UI_data

#yaml文件路径
yaml = os.path.join(datapath,'activity_search.yml')

#定义参数名称
args_item = 'url,send_data,resultstr'

#读取yaml文件加载测试数据
test_data, case_desc = OperationYaml(yaml).load_file


@pytest.mark.parametrize(args_item, test_data, ids = case_desc)
class Test_activity():

    @pytest.mark.activity_search
    def test_activity_search(self,browser,baseurl,url,send_data,resultstr):
        '''测试活动搜索'''
        page = businessoperate().login_success(browser,baseurl)
        browser.maximize_window()
        page.get(url)
        sleep(2)
        page2 = activity_mange(browser)
        page2.activity_name.send_keys(send_data['activity_name'])
        page2.sreach_btn.click()
        sleep(2)
        result = page2.result.text
        sleep(2)
        assert result in resultstr['resultstr']

if __name__ == '__main__':
    pytest.main(['-x',r'd:\MTQ\testcase\UI_case\test_activity_search.py'])