import pytest,os
from time import sleep
from operate.business_operate import businessoperate
from common.read_yaml import OperationYaml
from config import globalconfig
from page_object.Photo_album_management.album_search import album_search


#从配置文件读取测试数据路径
datapath = globalconfig.UI_data
#yaml文件路径
yaml = os.path.join(datapath,'album_search.yml')

args_item = 'url,send_data,resultstr'

#读取yaml文件加载测试数据
test_data, case_desc = OperationYaml(yaml).load_file

@pytest.mark.parametrize(args_item, test_data, ids = case_desc)
class Test_album():

    @pytest.mark.album_search
    def test_album_search(self,browser,baseurl,url,send_data,resultstr):
        '''
        测试相册搜索
        '''
        page = businessoperate().login_success(browser,baseurl)
        browser.maximize_window()
        page.get(url)
        sleep(5)
        page2 = album_search(browser)
        page2.album_number.send_keys(send_data['album_number'])
        page2.search_button.click()
        sleep(5)
        result = page2.result.text
        assert result in resultstr['resultstr']

if __name__ == '__main__':
    pytest.main([r'D:\MTQ\testcase\UI_case\test_activity_search.py','--tb=short'])
