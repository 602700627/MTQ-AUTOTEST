import requests,pytest
from config import globalconfig
from common.read_yaml import OperationYaml

from operate.interface_operate import interface_operate

# 从配置文件读取测试数据路径
baseurl = globalconfig.business_url


class Test_movement_delete():

    @pytest.mark.movement_delete
    def test_movement_delete(self):
        '''檬太奇删除动态'''
        url = baseurl + '/movement/delete'
        id = interface_operate().movement_create()
        payload = {
            'id':'%s'%id
        }
        r = requests.get(url,params=payload)
        print(r.json())
        rsl = r.json()['message']
        assert rsl == '删除成功'


if __name__ == '__main__':
    Test_movement_delete().test_movement_delete()