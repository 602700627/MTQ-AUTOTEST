import requests,pytest
from config import globalconfig
from common.read_yaml import OperationYaml

from operate.interface_operate import interface_operate

# 从配置文件读取测试数据路径
baseurl = globalconfig.business_url

class Test_delete_user():

    @pytest.mark.delete_user
    def test_delete_user(self):
        '''檬太奇房主删除成员'''
        url = baseurl + '/wfa/activityRoom/deleteUser'
        history = interface_operate().saveBrowingHistory()

        payload = {
            'activityId':'hc-f-659357',
            'unionIds':'oYnHqszn5_jpN9aAgxCAQKYGYLIU'
        }
        r = requests.post(url,data=payload)
        print(r.json())
        rsl = r.json()['result']['data']
        assert rsl == '踢出操作成功'


if __name__ == '__main__':
    Test_delete_user().test_delete_user()