import requests,pytest
from config import globalconfig

baseurl = globalconfig.business_url

class Test_movement_praise():

    @pytest.mark.skip(reason="此功能需要前端在小程序调用微信登录后，才能点赞，小程序登录接口需要在小程序UI调用，目前在外部无法调用")
    @pytest.mark.movement_praise
    def test_movement_praise(self):
        '''小程序点赞动态'''


        url= baseurl+'/movement/praise'
        payload ={
            'discoverId':'1',
            'movementId': '169',
            'unionId': 'oYnHqs82GD8B6ceiztzIi3MkUbBU'
                }


        r = requests.post(url,data = payload)
        print(r.json())
        rsl = r.json()['code']
        assert 900 == rsl


if __name__ == '__main__':
    Test_movement_praise().test_movement_praise()