import requests,pytest
from config import globalconfig
import datetime
from common.read_yaml import OperationYaml

# 从配置文件读取测试数据路径
baseurl = globalconfig.business_url

class Test_fanslist():

    @pytest.mark.fanslist
    def test_fanslist(self):
        '''檬太奇个人中心查看粉丝列表'''
        url = baseurl +'/follows/list'

        payload ={
            'unionId':'oYnHqs82GD8B6ceiztzIi3MkUbBU'
        }

        r = requests.get(url,params=payload)
        print(r.json())
        msg = r.json()['message']
        assert msg == '操作成功'

if __name__ == '__main__':
    Test_fanslist().test_fanslist()