import requests,pytest,os
from config import globalconfig
from common.read_yaml import OperationYaml
from operate.interface_operate import interface_operate

# 从配置文件读取测试数据路径
baseurl = globalconfig.business_url


class Test_delete_photot():

    @pytest.mark.delete_photot
    def test_delete_photot(self):
        '''檬太奇在相册删除照片接口'''

        url = baseurl +'/admin/imageDetail/delete'

        #先在相册上传一张照片，取该照片的picID
        picID = interface_operate().upload_photo()

        payload = {
            'picId': '%s' % picID
        }

        r = requests.post(url, data=payload)
        print(r.json())
        rsl = r.json()['code']
        assert rsl == 200


