import requests,pytest
from config import globalconfig

baseurl = globalconfig.business_url

class Test_create_albums():

    @pytest.mark.create_albums
    def test_create_albums(self):
        '''小程序创建相册'''
        url = baseurl + '/activityRoom/save'
        payload = {
            'activityName':'活动相册',
            'activityType':'活动相册',
            'startTime':'2019-01-01 00:00:00',
            'endTime':'2019-12-30 00:00:00',
            'unionId':'oYnHqs3FBdLwZwj6B_99jV1Pc86Q',
            'powerType':'1',
            'activityStyle':'10'
        }

        r = requests.post(url,data = payload)

        rsl = r.json()['message']

        assert '操作成功' == rsl


if __name__ == '__main__':
    Test_create_albums().test_create_albums()