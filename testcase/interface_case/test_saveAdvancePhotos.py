import requests,pytest
from config import globalconfig
import datetime
from common.read_yaml import OperationYaml
from common.random_number_generation import create_phone

from operate.interface_operate import interface_operate

# 从配置文件读取测试数据路径
baseurl = globalconfig.business_url

class Test_saveAdvancePhotos():

    @pytest.mark.skip(reason='暂时没调通')
    @pytest.mark.saveAdvancePhotos
    def test_saveAdvancePhotos(self):
        '''檬太奇保存预约摄影师信息'''
        url = baseurl + '/wfa/activityRoom/deleteUser'
        phone = interface_operate()
        startTime = datetime.datetime.now()
        endTime = (startTime + datetime.timedelta(days=7)).strftime('%Y-%m-%d')
        payload = {
            'mobileNo':'%s'%phone,
            'unionId':'oYnHqszn5_jpN9aAgxCAQKYGYLIU',
            'startTime':'%s'%startTime,
            'endTime':'%s'%endTime,
            'userName':'接口自动化测试预约摄影师'
        }

        r = requests.post(url,data=payload)
        print(r.json())


if __name__ == '__main__':
    Test_saveAdvancePhotos().test_saveAdvancePhotos()