import requests,pytest,os
from config import globalconfig
from common.read_yaml import OperationYaml
from operate.interface_operate import interface_operate

# 从配置文件读取测试数据路径
baseurl = globalconfig.business_url


class Test_get_photot_list():

    @pytest.mark.get_photot_list
    def test_get_photot_list(self):
        '''檬太奇在相册获取照片瀑布流'''

        url = baseurl +'/maggieAblum/getWaterfallByActivityId'

        payload = {
            'activityId':'hc-f-686041'
        }

        r = requests.get(url, params=payload)
        #print(r.json())

        rsl = r.json()['code']
        assert rsl == 200


if __name__ == '__main__':
    Test_get_photot_list().test_get_photot_list()