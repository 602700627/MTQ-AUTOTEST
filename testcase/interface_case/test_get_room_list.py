import requests,pytest,os
from config import globalconfig
from common.read_yaml import OperationYaml

# 从配置文件读取测试数据路径
datapath = globalconfig.interface_data

yaml = os.path.join(datapath,'get_room_list.yml')

# 定义参数名称
args_item = 'url,send_data,resultstr'

# 读取yaml文件加载测试数据
test_data, case_desc = OperationYaml(yaml).load_file

@pytest.mark.parametrize(args_item,test_data,ids=case_desc)
class Test_get_room_list():

    @pytest.mark.get_room_list
    def test_get_room_list(self,url,send_data,resultstr):
        '''檬太奇查询用户房间列表'''

        payload = send_data

        r = requests.get(url, params= payload)
        print(r.json())
        rsl = r.json()['message']
        assert rsl in resultstr['resultstr']

