
'''
此位置主要用于封装浏览器的基本操作,例如切换句柄，抓取屏幕文字等
'''

class BaseOperate(object):
    def radio_buttons(self,elm):
        '''
        处理单项选择框
        :param elm: 传入单项选择框元素
        :return:
        '''
        try:
            for i in elm:
                i.click()
            #print("选中单选框")
        except Exception as e:
            print('fail', format(e))


    def this_handle(self,brower):
        '''
        获取当前窗口句柄
        :param elm: 传入浏览器驱动
        :return:
        '''
        return brower.current_window_handle

    def all_handles(self,brower):
        '''
        获取所有窗口句柄
        :param elm: 传入浏览器驱动
        :return:
        '''
        return brower.window_handles

    def switch_handle(self,brower,frist_handle,all_handles):
        '''
        切换句柄
        :param elm: 传入窗口句柄
        :return:
        '''
        for handle in all_handles:
            if handle != frist_handle:
                brower.switch_to.window(handle)
        print('Window switched!')

    def switch_frame(self):
        pass
