# -*- coding: utf-8 -*-
from page_object.login_page.login_page import login_page
from config import globalconfig
from selenium import webdriver

'''
此位置主要用于完成特定业务操作
'''
username = globalconfig.login_user
pwd = globalconfig.login_pwd
baseurl = globalconfig.baseurl

class businessoperate():

    def login_success(self,browser,baseurl):
        '''登录'''
        page = login_page(browser)
        page.get(baseurl)
        page.username.send_keys(username)
        page.password.send_keys(pwd)
        page.login_box.click()
        return page


if __name__ == '__main__':
    dr = webdriver.Chrome()
    businessoperate().login_success(dr,baseurl)